
require('module-alias/register');

const chai = require('chai');
const chaihttp = require('chai-http');
const should = require('should/as-function');
chai.use(chaihttp);

const web = require('@src/server.js');

async function serverPromise(config) {
  const app = await web(config, [
    { method: 'get', path: '/api/test', function: (req, res) => { res.respond(200, {"hello":"there"}, ''); } }
  ]);

  return app;
}

var config = { test: true };

describe('Generic',() =>  {
  let server;
  
  before(async () => {
    return serverPromise(config).then(app => {
      server = app;
    });
  });

  after(() => {

  });

  beforeEach(() => {

  });

  afterEach(() => {

  });

  describe('#health',() => {
    it('should pass healthcheck',(done) => {
      chai.request(server)
      .get('/health')
      .send('{}')
      .end((err,res) => {
        should(res).have.property('status');
        should(res.status).be.exactly(200);
        done();
      });
    });
  });
});
