
require('module-alias/register');

process.on('uncaughtException', function(error) {
  console.log(JSON.stringify(error, Object.getOwnPropertyNames(error)));
});

process.on('unhandledRejection', (error, promise) => {
  console.log(JSON.stringify(promise, Object.getOwnPropertyNames(promise)));
  console.log(JSON.stringify(error, Object.getOwnPropertyNames(error)));
});

const web = require('@src/server.js');

console.log('{"message": "reading config file..."}');
const config = require('@config/config.json');

async function start() {
  const app = await web(config, [
    { method: 'get', path: '/api/test', function: (req, res) => { res.respond(200, {"msg":"success"}, ''); } },
    { method: 'get', path: '/api/404', function: (req, res) => { res.respond(404, {"hello":"not found"}, ''); } },
    { method: 'get', path: '/api/500', function: (req, res) => { res.respond(500, {"hello":"server error"}, ''); } },
    { method: 'get', path: '/api/503', function: (req, res) => { res.respond(500, {"hello":"server unavailable"}, ''); } }
  ]);

  return app;
}

console.log('{"message": "starting web app..."}');
start();
