
"use strict";

const _ = require('lodash');
const cors = require('cors');
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');
const session = require('express-session');
const compression = require('compression');
const busboy = require('connect-busboy');
const http = require('http');
const https = require('https');
const http2 = require('http2');
const express = require('express');
const morgan = require('morgan');
const pem = require('pem');
const prometheus = require('prom-client');
const redis = require("redis");
const postgres = require('pg');
const mysql = require('mysql');
const memcached = require('memcached');
const elasticsearch = require('elasticsearch');
const opentracing = require('opentracing');
const jaeger = require('jaeger-client');
const apm = require('elastic-apm-node');

/*
  Sets up the following endpoints on http & https (self-signed):
    /metrics  => prometheus metrics on port 5000
    /health   => health check
    /*        => 404 not found
  
  Enables:
    - json access logs
    - content-type auto-processing: application/json & application/x-www-form-urlencoded
    - cookie auto-processing
    - session auto-processing
  
  Creates:
    request.respond(status, data, err) for clean-looking responses
*/
function server(config, routes) {
  return new Promise(async (resolve, reject) => {
    /* for indicating all servers are running */
    var app;
    var srvCnt = 0;
    function serverReady() {
      srvCnt++;
      if (srvCnt === 3) resolve(app);
    }

    /* set config defaults */
    const CONFIG = _.defaultsDeep(config, {
      service: 'app',
      db: {
        redis: [],
        postgres: [],
        mysql: [],
        memcached: [],
        elasticsearch: []
      },
      dev: false,
      metrics: {
        prefix: '',
        timeout: 5000,
        port: 5000
      },
      tracing: {
        url: ''
      },
      apm: {
        token: '',
        url: ''
      },
      http: {
        port: 8080
      },
      https: {
        port: 8443
      },
      session: {
        secret: Math.random().toString(36).substring(11),
        resave: false,
        saveUninitialized: true,
        cookie: { secure: true }
      },
      cors: {
        enable: false,
        origins: [], // empty array => allow all origins
        options: {
          origin: "*",
          methods: "GET,HEAD,PUT,PATCH,POST,DELETE",
          preflightContinue: false,
          optionsSuccessStatus: 204
        }
      },
      busboy: {},
      set: {}
    });

    /******************************** server init ********************************/

    /* get express server object */
    app = express();

    /* set app variables */
    for (let key in CONFIG.set) {
      app.set(key, CONFIG.set[key]);
    }
      
    /* disable this http header */
    app.disable('x-powered-by');

    /* custom middleware response function */
    app.use((req, res, next) => {
      res.respond = (status, data, err) => {
        return res.status(status).json({'data': data, 'error': String(err)});
      };
      next();
    });

    /* error handling middleware */
    app.use((err, req, res, next) => {
      res.respond(500, {}, err);
    });

    /******************************** metrics ********************************/

    prometheus.collectDefaultMetrics({ prefix: CONFIG.metrics.prefix, timeout: CONFIG.metrics.timeout });

    const metricsServer = express();
    metricsServer.get('/metrics', (request, response) => {
      response.set('Content-Type', prometheus.register.contentType);
      response.end(prometheus.register.metrics());
    });
    http.createServer(metricsServer).listen(CONFIG.metrics.port, () => {
      console.info(`{"message": "metrics server listening at ${CONFIG.metrics.port}"}`);
      serverReady();
    });

    /******************************** logs ********************************/

    /* enable morgan for json access logs */
    morgan.token('body', (req, res) => { return req.body; });
    const morganJSON = (tokens, req, res) => {
      return JSON.stringify({
        'remote-address': tokens['remote-addr'](req, res),
        'time': tokens['date'](req, res, 'iso'),
        'method': tokens['method'](req, res),
        'url': tokens['url'](req, res),
        'http-version': tokens['http-version'](req, res),
        'status-code': tokens['status'](req, res),
        'content-length': tokens['res'](req, res, 'content-length'),
        'referrer': tokens['referrer'](req, res),
        'user-agent': tokens['user-agent'](req, res),
        'body': tokens['body'](req, res)
      });
    }
    app.use(morgan(morganJSON));

    /******************************** traces ********************************/

    if (CONFIG.tracing.url) {
      const tlogger = {
        info: (str) => {
          console.log(JSON.stringify({message: str}));
        },
        error: (str) => {
          console.error(JSON.stringify({error: str}));
        }
      };
      
      const tracer = jaeger.initTracer({
        serviceName: CONFIG.service,
        reporter: {
          collectorEndpoint: CONFIG.tracing.url,
          logSpans: false
        },
        sampler: {
          type: opentracing.SAMPLER_TYPE_PROBABILISTIC,
          param: 0.5
        }
      }, {
        tags: {
          'version': process.env.npm_package_version,
        },
        metrics: new jaeger.PrometheusMetricsFactory(prometheus, CONFIG.service),
        logger: tlogger
      });

      app.use((req, res, next) => {
        let span = tracer.startSpan(req.path);
        span.log({'event': 'request_start'});
        res.on('error', err => {
          span.setTag(opentracing.Tags.ERROR, true);
          span.log({'event': 'error', 'error.object': err, 'message': err.message, 'stack': err.stack});
          span.finish();
        });
        res.on('end', () => {
          span.log({'event': 'request_end'});
          span.finish();
        });
        next();
      });
    }

    /******************************** apm ********************************/

    if (CONFIG.apm.token) {
      apm.start({
        serviceName: CONFIG.service,
        secretToken: CONFIG.apm.token,
        serverUrl: CONFIG.apm.url,
        active: true,
        captureBody: 'errors',
        captureHeaders: true,
        captureEnv: true,
        metricsInterval: '30s'
      });
    }

    /******************************** server config ********************************/

    /* custom middleware for dev issues */
    if (CONFIG.cors.enable > 0) {
      app.options('*', cors(CONFIG.cors.options));

      if (CONFIG.cors.origins.length > 0) {
        CONFIG.cors.options.origin = (origin, callback) => {
          if (CONFIG.cors.origins.indexOf(origin) !== -1) {
            callback(null, true);
          } else {
            callback(new Error('Domain not allowed by CORS'), false);
          }
        };
      }

      app.use(cors(CONFIG.cors.options));
    }

    /* enable gzip compression, json & x-www-form-urlencoded bodies, cookies, and sessions */
    app.use(compression());
    app.use(busboy(CONFIG.busboy));
    app.use(bodyParser.json());
    app.use(bodyParser.urlencoded({ extended: true }));
    app.use(cookieParser());
    app.use(session(CONFIG.session));

    /**
      request:
    */
    app.get('/health', (req, res) => {
      res.respond(200, {}, '');
    });

    /* set routes */
    for (let route of routes) {
      app[route.method](route.path, route.function);
      console.info(`{"message": "set route: ${route.method} -> ${route.path}"}`);
    }

    /**
      request:
    */
    app.all('*',(req, res) => {
      res.respond(404, {}, 'Not Found');
    });

    /******************************** database ********************************/

    let connections = [];

    for (let db of CONFIG.db.redis) {
      console.info(`{"message": "connection: ${db.host} ` + db.password.substring(0,5) + '"}');
      let client = redis.createClient({
        host: db.host,
        password: db.password,
        port: 6379
      });
      connections.push(client);
      client.quit();
    }

    for (let db of CONFIG.db.postgres) {
      console.info(`{"message": "connection: ${db.host} ${db.name} ${db.user} ` + db.password.substring(0,5) + '"}');
      let client = new postgres.Client({
        user: db.user,
        host: db.host,
        database: db.name,
        password: db.password,
        port: 5432
      });
      await client.connect();
      connections.push(client);
      await client.end()
    }

    for (let db of CONFIG.db.mysql) {
      console.info(`{"message": "connection: ${db.host} ${db.name} ${db.user} ` + db.password.substring(0,5) + '"}');
      let client = mysql.createConnection({
        user: db.user,
        host: db.host,
        database: db.name,
        password: db.password,
        port: 3306
      });
      client.connect();
      connections.push(client);
      client.end();
    }

    for (let db of CONFIG.db.memcached) {
      console.info(`{"message": "connection: ${db.host}"}`);
      memcached.connect(`${db.host}:11211`, (err, client) => {
        if (err) throw new Error(err);
        connections.push(client);
      });
    }

    for (let db of CONFIG.db.elasticsearch) {
      console.info(`{"message": "connection: ${db.host} ${db.user} ` + db.password.substring(0,5) + '"}');
      let client = new elasticsearch.Client({
        host: `${db.host}:9200`,
        auth: {
          username: `${db.user}`,
          password: `${db.password}`
        }
      });
      client.ping({ requestTimeout: 1000 }, (err) => {
        if (err) throw new Error(err);
      });
    }

    /******************************** server start ********************************/

    /* start http server */
    app.server = http.createServer(app);
    app.server.listen(CONFIG.http.port, () => {
      console.info('{"message": "http server listening at port ' + CONFIG.http.port + '"}');
      serverReady();
    });

    /* start https server */
    pem.createCertificate({
      days: 365,
      selfSigned: true
    }, (err, keys) => {
      if (err) throw err;

      //// expressjs doesn't support http2 yet: https://github.com/expressjs/express/pull/3730
      // http2.createSecureServer({
      //   key: keys.serviceKey,
      //   cert: keys.certificate,
      //   allowHTTP1: true
      // }).listen(CONFIG.https.port, () => {
      //   console.info("https server listening at port " + CONFIG.https.port);
      //   serverReady();
      // });

      https.createServer({
        key: keys.serviceKey,
        cert: keys.certificate
      }, app).listen(CONFIG.https.port, () => {
        console.info('{"message": "https server listening at port ' + CONFIG.https.port + '"}');
        serverReady();
      });
    });
  });
};

module.exports = server;
